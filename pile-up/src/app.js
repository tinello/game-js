/* global document */
/* global window */
/* global main */

const pista = document.getElementById('pista');
let frameCounter = 0;
let screen;
let carPosition = 2;
const carSimbol = '&#128664;';
const crashSimbol = '&#128293;';

const carrera = [];
for (let i = 0; i < 1; i += 1) {
  carrera.push(['*', '', '', '', '']);
  carrera.push(['', '', '', '', '']);
  carrera.push(['', '', '*', '', '']);
  carrera.push(['', '', '', '', '']);
  carrera.push(['', '*', '*', '*', '*']);
  carrera.push(['', '', '', '', '']);
  carrera.push(['', '', '', '*', '']);
  carrera.push(['', '', '*', '', '']);
  carrera.push(['*', '', '', '', '']);
  carrera.push(['', '', '', '', '']);
  carrera.push(['', '', '*', '', '']);
  carrera.push(['', '', '', '', '']);
  carrera.push(['', '', '', '', '']);
  carrera.push(['', '', '', '', '*']);
  carrera.push(['', '', '', '', '']);
}

document.addEventListener('keydown', (event) => {
  /* const key = event.which || event.keyCode || 0; */
  const { code } = event;
  switch (code) {
    case 'ArrowLeft':
      if (carPosition > 0) {
        carPosition -= 1;
      }
      break;
    case 'ArrowRight':
      if (carPosition < 4) {
        carPosition += 1;
      }
      break;
    default:
  }
});


window.main = () => {
  window.requestAnimationFrame(main);

  if (frameCounter === 0) {
    // Primer render de la pista.
    screen = carrera.slice(carrera.length - 10, carrera.length);
  } else {
    const draw = JSON.parse(JSON.stringify(screen));
    const lastStep = draw[draw.length - 1];

    if (lastStep[carPosition] === '*') {
      lastStep[carPosition] = crashSimbol;
    } else {
      lastStep[carPosition] = carSimbol;
    }

    let html = '';
    for (let i = 0; i < draw.length; i += 1) {
      html += '<tr>';
      const row = draw[i];
      for (let j = 0; j < row.length; j += 1) {
        const column = row[j];
        const icon = column === '*' ? '&#128660;' : column;
        html += `<td width="25" height="25">${icon}</td>`;
      }
      html += '</tr>';
    }

    pista.innerHTML = html;
  }

  if (frameCounter >= 30) {
    screen = carrera.slice(carrera.length - 10, carrera.length);
    carrera.unshift(carrera.pop());
    frameCounter = 1;
  }
  frameCounter += 1;
};

main(); // Empezar el ciclo
